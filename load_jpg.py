# Typical setup to include TensorFlow.
import tensorflow as tf
import numpy as np

def img_reader_filename_per_class(filename_paths, batch_size_per_class, resized_shape=[224, 224], rgb2gray=True, num_preprocess_threads=1, min_queue_examples=16):

    """Build image reader function filename per class.

    Args:
        filename_paths (list) : List of directories of images.
           directory is below,
           .
           |-- data_folder
              |-- class1 name
                 |-- 0.jpg
                 |-- 1.jpg ...
              |-- class2 name
                 |-- 0.jpg
                 |-- 1.jpg ...
              |-- class3 name
                 |-- 0.jpg
                 |-- 1.jpg ...
        batch_size_per_class (int): batch size per class. if you enter 4 and number of class is 3, total batch size is that 4*3 = 12.
        resized_shape (list): shapes to resize (height, width and channel of image). Defaults to [224, 224].
        rgb2gray (bool): whether rgb to gray scaling. Defaults to True.
        num_preprocess_threads (int): number of using threads for preprocessing. Defaults to 1.
        min_queue_examples (int): Minimum number elements in the queue after a dequeue, used to ensure a level of mixing of elements. Defaults to 16

    Returns:
        tf.Tensor: Tensor by tf.train.shuffle_batch per class.
    """
    images_per_class = []
    image_reader = tf.WholeFileReader()
    for i in range(len(filename_paths)):
        file_path = filename_paths[i]
        filename_queue = tf.train.string_input_producer(
            tf.train.match_filenames_once(file_path)
        )
        _, image_file = image_reader.read(filename_queue)
        image = tf.image.decode_jpeg(image_file)
        image = tf.image.resize_images(image, resized_shape) if resized_shape is not None else next
        image = tf.image.rgb_to_grayscale(image) if rgb2gray else next
        set_shapes = resized_shape + [int(rgb2gray)] if rgb2gray else resized_shape + [int(rgb2gray) + 2]
        image.set_shape(set_shapes)
        images = tf.train.shuffle_batch(
             [image],
             batch_size=batch_size_per_class,
             num_threads=num_preprocess_threads,
             capacity=min_queue_examples + 3 * batch_size_per_class,
             min_after_dequeue=min_queue_examples)
        images_per_class.append(images)
    return images_per_class