import matplotlib.pyplot as plt
import numpy as np

def show_images(images, cols = 1, titles = None):
    """Display a list of images in a single figure with matplotlib.

    Args:
        images (ndarray):  List of np.arrays compatible with plt.imshow.
        cols (int):  Number of columns in figure (number of rows is set to np.ceil(n_images/float(cols))). Defaults to 1.
        titles (list):  List of titles corresponding to each image. Must have the same length as titles. Defaults to None.

    Raises:
        AssertionError: if len(titles) is not equal to len(images).
    """
    assert((titles is None)or (len(images) == len(titles)))
    n_images = len(images)
    if titles is None: titles = ['Image (%d)' % i for i in range(1,n_images + 1)]
    fig = plt.figure()
    for n, (image, title) in enumerate(zip(images, titles)):
        a = fig.add_subplot(cols, np.ceil(n_images/float(cols)), n + 1)
        if image.ndim == 2:
            plt.gray()
        plt.imshow(image)
        a.set_title(title)
    fig.set_size_inches(np.array(fig.get_size_inches()) * n_images)
    plt.show()